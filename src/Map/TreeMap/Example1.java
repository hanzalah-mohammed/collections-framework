package Map.TreeMap;

// TreeMap are always sorted based on the natural ordering of the keys,
// or based on a custom Comparator that you can provide at the time of creation of the TreeMap.

import java.util.TreeMap;

public class Example1 {
    public static void main(String[] args) {
        TreeMap<Integer, String> refMap = new TreeMap<>();

        refMap.put(101, "James");
        refMap.put(102, "Alex");
        refMap.put(103, "Sam");
        refMap.put(104, "John");
        refMap.put(105, "Amit");
        System.out.println(refMap);
    }
}
