package Map.HashMap;

// Unlike ArrayList, HashMap store items in "key/value" pairs,
// and you can access them by an index of another type (e.g. a String).

import java.util.HashMap;

public class Example1 {
    public static void main(String[] args) {

        HashMap<Integer, String> refMap = new HashMap<>();
        refMap.put(103, "James");
        refMap.put(102, "Alex");
        refMap.put(101, "Sam");
        refMap.put(105, "John");
        refMap.put(99, "Amit");
        System.out.println(refMap);

        System.out.println(refMap.get(101)); // .get() prints the value of the key

        refMap.keySet().forEach(System.out::println); // use .keySet() to get the keys only
        refMap.values().forEach(System.out::println); // use .values() to get the values only
        refMap.entrySet().forEach(System.out::println); // use .entrySet() to get both keys & values

        for (int key : refMap.keySet()){
            System.out.println("ID " + key + ": " + refMap.get(key));
        }
    }
}
