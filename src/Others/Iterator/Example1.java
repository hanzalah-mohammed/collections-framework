package Others.Iterator;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

public class Example1 {
    public static void main(String[] args) {
        List<Integer> refList = new ArrayList<>();

        Collections.addAll(refList, 1,2,3,4,5,6,7,8,9,10);
        System.out.println(refList);

        Iterator<Integer> refIterator = refList.iterator();

        // this is the same as for loop
//        while (refIterator.hasNext()){
//            System.out.println(refIterator.next());
//        }

        // use iterator to remove items
        // as trying to remove items using a for loop or a for-each loop would not work correctly,
        // because the collection is changing size at the same time that the code is trying to loop.
        while (refIterator.hasNext()){
            if (refIterator.next() % 2 != 0){
                refIterator.remove();
            }
        }

        // the other method to remove elements
        // refList.removeIf(e -> e % 2 != 0);

        System.out.println(refList);
    }
}
