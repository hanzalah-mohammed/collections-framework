package Others.Sort.Comparator;

// create comparator using Lambda Expression

import Data.Person;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class Example2 {
    public static void main(String[] args) {

        // Object
        List<Person> refList = new ArrayList<>();
        refList.add(new Person(4, "Alex"));
        refList.add(new Person(2, "Danny"));
        refList.add(new Person(3, "Sam"));
        refList.add(new Person(1, "Gabriel"));
        refList.add(new Person(5, "Lina"));

        System.out.println("\nBefore sorting: ");
        System.out.println(refList);

        // Comparator using Lambda Expression
        // Define your comparator - how do you want to sort the objects
        // Comparator has different comparing() methods depends on what value we want to compare and sort
        Comparator<Person> sortByID = Comparator.comparingInt(o -> o.ID); // .comparingInt for Integers
        Comparator<Person> sortByName = Comparator.comparing(o -> o.name); // .comparing() for String

        // To sort list of objects -> CollectionsRef.sort(user-defined comparator)
        System.out.println("After sorting: ");
        refList.sort(sortByID); // insert the comparator in the sort() method.
        refList.sort(sortByName); // insert the comparator in the sort() method.
        System.out.println(refList);
    }
}
