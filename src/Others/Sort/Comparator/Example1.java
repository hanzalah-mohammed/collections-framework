package Others.Sort.Comparator;

// create comparator using Anonymous inner class

import Data.Person;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class Example1 {
    public static void main(String[] args) {

        // Object
        List<Person> refList = new ArrayList<>();
        refList.add(new Person(4, "Alex"));
        refList.add(new Person(2, "Danny"));
        refList.add(new Person(3, "Sam"));
        refList.add(new Person(1, "Gabriel"));
        refList.add(new Person(5, "Lina"));

        System.out.println("\nBefore sorting: ");
        System.out.println(refList);

        // Comparator using Anonymous inner class
        // Define your comparator - how do you want to sort the objects
        Comparator<Person> comparator = new Comparator<>() {
            @Override
            public int compare(Person o1, Person o2) {
                return o1.ID - o2.ID; // sort using ID
                // return o1.name.compareTo(o2.name); // sort using Name
            }
        };

        System.out.println("After sorting: ");
        refList.sort(comparator); // insert the comparator in the sort() method.
        System.out.println(refList);
    }
}
