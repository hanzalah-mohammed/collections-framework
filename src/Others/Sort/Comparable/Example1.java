package Others.Sort.Comparable;

// using Collections.sort() on 1 data type only

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class Example1 {
    public static void main(String[] args) {

        // Integers
        List<Integer> refList = Arrays.asList(2, 1, 4, 4, 3);

        System.out.println("\nBefore sorting: ");
        System.out.println(refList);

        System.out.println("After sorting: ");
        Collections.sort(refList); // by default sort will just sort in ascending order on 1 data type.
        System.out.println(refList);

        // String
        List<String> refList2 = Arrays.asList("James", "Lisa", "Bryan", "John", "Danny", "Alex");

        System.out.println("\nBefore sorting: ");
        System.out.println(refList2);

        System.out.println("After sorting: ");
        Collections.sort(refList2); // by default sort will just sort in ascending order on 1 data type.
        System.out.println(refList2);

        // Object
        // object cannot be sorted unless using comparator - JVM needs to know how to sort
        List<Object> refList3 = Arrays.asList(123, "Lisa", 456, "Alexia", 789);

        System.out.println("\nBefore sorting: ");
        System.out.println(refList3);

        System.out.println("After sorting: ");
        // Collections.sort(refList3); // you will get compilation error
        System.out.println(refList3);
    }
}
