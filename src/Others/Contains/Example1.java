package Others.Contains;

// check using contains() based on primitive data type

import java.util.ArrayList;
import java.util.List;

public class Example1 {
    public static void main(String[] args) {

        // Integer
        List<Integer> refList = new ArrayList<>();
        refList.add(2);
        refList.add(1);
        refList.add(4);
        refList.add(4);
        refList.add(3);
        System.out.println("\n" + refList);

        System.out.println("List contains 9? " + refList.contains(9));
        System.out.println("List contains 3? " + refList.contains(3));

        //String
        List<String> refList2 = new ArrayList<>();
        refList2.add("James");
        refList2.add("Alex");
        refList2.add("Bryan");
        refList2.add("Sammy"); // duplicate values wont be removed
        refList2.add("Sammy"); // duplicate values wont be removed
        refList2.add("John");
        System.out.println("\n" + refList2);

        System.out.println("List contains Lisa? " + refList2.contains("Lisa"));
        System.out.println("List contains John? " + refList2.contains("John"));
    }
}

