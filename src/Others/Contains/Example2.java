package Others.Contains;

// check using contains() based on object

import java.util.ArrayList;
import java.util.List;

import Data.Person;

public class Example2 {
    public static void main(String[] args) {

        boolean available;

        List<Person> refList = new ArrayList<>();

        refList.add(new Person(1, "Hanz"));
        refList.add(new Person(3, "Kok Weng"));
        refList.add(new Person(2, "Timothy"));
        refList.add(new Person(4, "Gabriel"));
        System.out.println(refList);

        // new created objects will have their own memory address.
        // to check objects in list with same values, we must generate hashCode() and equals() in Object class.
        System.out.println("\nList contains Lisa?");
        available = refList.contains(new Person(1, "Lisa"));
        System.out.println(available);

        System.out.println("\nList contains Hanz?");
        available = refList.contains(new Person(1, "Hanz"));
        System.out.println(available);
    }
}
