package Others.Stream;

// source -> operation -> result
// operation is intermediate operations such as filter(), limit(), sort(), map()
// result is terminal operations which can use 1 time such as reduce(), collect(), min() and max()

import Data.Employee;

import java.util.*;
import java.util.stream.Collectors;

public class Operations {
    public static void main(String[] args) {

        // Sample list
        List<Employee> empList = new ArrayList<>();
        empList.add(new Employee(1, "charlie", 3000.50));
        empList.add(new Employee(2, "amin", 2000));
        empList.add(new Employee(3, "Danny", 1000));
        empList.add(new Employee(4, "Bryan", 4250));

        // examples of common intermediate operations

        System.out.println("\nUsing filter()");
        // filter()
        empList.stream().filter(e -> e.salary > 2000 && e.name.charAt(0) == 'C').forEach(System.out::println);

        System.out.println("\nUsing limit() - ascending order");
        // limit() - will get in ascending order
        empList.stream().limit(2).forEach(System.out::println);

        System.out.println("\nUsing sort() - Alphabetical");
        // sort() -> you must include comparator -> .sort(comparator)
        // when sorting string, remember 'A' and 'a' is different, and Capital letters is sorted first than non-capital
        // Before sorting, map all to lowercase, in this way it will sorted correctly.
        Comparator<Employee> sortByName = Comparator.comparing(e -> e.name.toLowerCase());
        empList.stream().sorted(sortByName).forEach(System.out::println);

        System.out.println("\nReversed order - by ID");
        Comparator<Employee> sortByID = Comparator.comparingInt(e -> e.ID);
        empList.stream().sorted(sortByID.reversed()).forEach(System.out::println);

        System.out.println("\nMax and Min");
        // max(), min(), average() and reduce()
        // you have to provide as Optional in case the result is not available
        // after that you can use OptionalClassRef.ifPresent()
        Comparator<Employee> sortBySalary = Comparator.comparingDouble(e -> e.salary); // will sort from lowest to highest
        Optional<Employee> highestSalary = empList.stream().max(sortBySalary);
        Optional<Employee> lowestSalary = empList.stream().min(sortBySalary);
        highestSalary.ifPresent(employee -> System.out.println("Highest Salary: " + employee));
        lowestSalary.ifPresent(employee -> System.out.println("Lowest Salary: " + employee));

        // mapToDouble() to convert the type of list to double, before we can use the average() and reduce() method.
        // we use OptionalDouble for average() due to the possibility of getting decimal.
        OptionalDouble averageSalary = empList.stream().mapToDouble(e -> e.salary).average();
        averageSalary.ifPresent(e -> System.out.println("Average Salary: " + e));

        // reduce() will sum all the values -> .reduce(starting value, (e1, e2) -> e1 + e2)
        double sumAllSalary = empList.stream().mapToDouble(e -> e.salary).reduce(0, Double::sum);
        System.out.println("Sum of Salary: " + sumAllSalary);

        System.out.println("\nMap");
        // map() - apply the logic to all the elements
        empList.stream().map(e -> e.name.toUpperCase()).forEach(System.out::println);

        System.out.println("\nCollect");
        // collect() - to put the stream to a list collection -> collect(Collectors.to<typeOfCollection>) eg. toList
        List<Employee> sortedByNameList = empList.stream().sorted(sortByName).collect(Collectors.toList());
        System.out.println("sortedByNameList: " + sortedByNameList);

        // can collect it to Set also -> use argument as Collectors.toSet()
        Set<Employee> salaryBetween1and4 = empList.stream().filter(e -> e.salary > 1000 && e.salary < 4000).collect(Collectors.toSet());
        System.out.println("salaryBetween1and4: " + salaryBetween1and4);
    }
}
