package Others.Stream;

// source -> operation -> result
// How to create a stream? from array and collections
// Stream does not modify the original array or collections

import Data.Employee;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Source {
    public static void main(String[] args) {

        // Fixed Array
        Employee [] arr = new Employee[3];
        arr[0] = new Employee(1, "John", 1000);
        arr[1] = new Employee(2, "Henry", 2000);
        arr[2] = new Employee(3, "Sammy", 3000);

        // create stream from array -> use Stream.of() -> Stream.of(array)
        Stream.of(arr).forEach(System.out::println);

        // Collection Arrays
        List<Employee> empList = new ArrayList<>();
        empList.add(new Employee(1, "John", 1000));
        empList.add(new Employee(2, "Henry", 2000));
        empList.add(new Employee(3, "Sammy", 3000));

        // create stream from Collection -> use .stream() method -> list.stream()
        empList.stream().limit(3).forEach(System.out::println);

        // Even after operations, your array & collections still remain the same
        List<Employee> newList = empList.stream().filter(e -> e.salary > 1000).limit(1).collect(Collectors.toList());
        System.out.println(newList); // stream will create new stream
        System.out.println(empList); // Your original list still remains the same
    }
}
