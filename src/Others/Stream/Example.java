package Others.Stream;

// How to get the 2nd smallest and 2nd largest number from unsorted list that contains duplicate values?
// By using stream, it is easy to achieve this.

import java.util.*;
import java.util.stream.Collectors;

public class Example {
    public static void main(String[] args) {

        // create sample of unsorted list with duplicate values
        List<Integer> number = Arrays.asList(2, 3, 1, 1, 5, 4, 4, 7, 9, 3, 5, 6, 1, 6, 2, 8, 4, 8);

        List<Integer> newList = number // call the source - collection / array
                .stream()   // creates new stream from the source
                .sorted()   // sorts the stream in ascending order by default
                .distinct() // removes duplicates from the stream
                .collect(Collectors.toList()); // puts the stream to a list collection -> collect(Collectors.toList())

        // newList is now sorted and contains no duplicate values, so starting is the smallest and ending is the largest value.
        int secondSmallest = newList.get(1);
        int secondLargest = newList.get(newList.size()-2);

        System.out.println(number); // original list
        System.out.println(newList); // sorted modified list without duplicates using stream
        System.out.println("2nd Smallest Number: " + secondSmallest);
        System.out.println("2nd Largest Number: " + secondLargest);
    }
}
