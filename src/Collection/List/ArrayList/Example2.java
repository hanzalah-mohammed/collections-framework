package Collection.List.ArrayList;

// ArrayList - add Object

import java.util.ArrayList;
import java.util.List;

import Data.Person;

public class Example2 {
    public static void main(String[] args) {

        List<Person> refList = new ArrayList<>();
        refList.add(new Person(1, "Hanz"));
        refList.add(new Person(3, "Kok Weng"));
        refList.add(new Person(2, "Timothy"));
        refList.add(new Person(4, "Gabriel"));
        refList.add(new Person(4, "Gabriel")); // duplicate values wont be removed
        System.out.println(refList);
    }
}
