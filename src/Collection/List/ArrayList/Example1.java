package Collection.List.ArrayList;

// ArrayList - add Integer & String
// ArrayList does not sort the values and remove duplicates.
// An ArrayList class can act as a list only because it implements List only.
// ArrayList is better for storing and accessing data, while LinkedList is better for manipulating data.

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Example1 {
    public static void main(String[] args) {

        //Integer
        List<Integer> refList = Arrays.asList(2, 1, 4, 4, 3);
        System.out.println(refList);

        //String
        List<String> refList2 = Arrays.asList("James", "Bryan", "Alex", "Alex", "John");
        System.out.println(refList2);
    }
}
