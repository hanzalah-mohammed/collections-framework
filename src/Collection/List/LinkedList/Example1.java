package Collection.List.LinkedList;

// LinkedList with List & Queue
// LinkedList does not sort the values and remove duplicates.
// LinkedList class can act as a list and queue both because it implements List and Deque interfaces.
// LinkedList is better for manipulating data, while ArrayList is better for storing and accessing data.

import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

public class Example1 {
    public static void main(String[] args) {

        System.out.println("LinkedList with the reference of List");
        List<Integer> refList = new LinkedList<>();
        refList.add(2);
        refList.add(1);
        refList.add(4);
        refList.add(4); // duplicate values wont be removed
        refList.add(3);
        refList.add(0, 9); // add() can insert specific index as well
        System.out.println(refList);

        System.out.println("LinkedList with the reference of Queue");
        Queue<Integer> refQueue = new LinkedList<>(); // LinkedList implements Deque
        refQueue.add(2);
        refQueue.add(3);
        refQueue.add(4);
        refQueue.add(4); // duplicate values wont be removed
        refQueue.offer(1); // offer() is the same as add()
        System.out.println(refQueue);

        // With Queue, you can access queue methods such as peek(), pool()
        System.out.println(refQueue.peek()); // get the Head
        refQueue.poll(); // pool() removes the Head
        System.out.println(refQueue);
    }
}

