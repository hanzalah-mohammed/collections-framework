package Collection.Set.LinkedHashSet;

// LinkedHashSet - add Integer & String
// LinkedHashSet will remove duplicates but does not sort the values.
// LinkedHashSet maintains the insertion order of the elements.

import java.util.LinkedHashSet;
import java.util.Set;

public class Example1 {
    public static void main(String[] args) {

        // Integer
        Set<Integer> refSet = new LinkedHashSet<>();
        refSet.add(3);
        refSet.add(2);
        refSet.add(5);
        refSet.add(1);
        // refSet.add(2); // duplicate values will be removed
        System.out.println(refSet);

        //String
        Set<String> refSet2 = new LinkedHashSet<>();
        refSet2.add("D");
        refSet2.add("A");
        refSet2.add("Z");
        refSet2.add("B");
        // refSet2.add("A"); // duplicate values will be removed
        System.out.println(refSet2);

    }
}