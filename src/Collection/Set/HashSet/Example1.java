package Collection.Set.HashSet;

// HashSet - add Integer, String & Object
// HashSet will remove duplicates and not always sort the values
// HashSet does not provide any method to maintain the insertion order.

import Data.Person;

import java.util.HashSet;
import java.util.Set;

public class Example1 {
    public static void main(String[] args) {

        // Integer
        Set<Integer> refSet = new HashSet<>();
        refSet.add(3);
        refSet.add(2);
        refSet.add(5);
        refSet.add(1);
        // refSet.add(2); // duplicate values will be removed
        System.out.println(refSet);

        // String
        Set<String> refSet2 = new HashSet<>();
        refSet2.add("D");
        refSet2.add("A");
        refSet2.add("Z");
        refSet2.add("B");
        // refSet2.add("A"); // duplicate values will be removed
        System.out.println(refSet2);

        // Object
        Set<Person> refSet3 = new HashSet<>();

        refSet3.add(new Person(1, "Hanz"));
        refSet3.add(new Person(3, "Kok Weng"));
        refSet3.add(new Person(2, "Timothy"));
        refSet3.add(new Person(4, "Gabriel"));

        // by default duplicate object wont be removed as new created objects will have their own memory address.
        // to remove duplicate object sill same values, we must generate hashCode() and equals() in Object class.
        refSet3.add(new Person(4, "Gabriel"));

        System.out.println(refSet3);
    }
}
