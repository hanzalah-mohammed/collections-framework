package Collection.Set.TreeSet;

// TreeSet - add integer and String
// TreeSet will sort the values in ascending order and remove duplicates,
// but TreeSet can only accept ONE data type
// You'll get compilation error if you try adding different type

import java.util.Set;
import java.util.TreeSet;

public class Example1 {
    public static void main(String[] args) {

        // Integer
        // <Object> in HashSet can accept both Integer & String, but not in TreeSet
        Set<Object> refSet = new TreeSet<>();
        refSet.add(1);
        refSet.add(3);
        refSet.add(2);
        refSet.add(9);
        refSet.add(5);
        refSet.add(7);
        refSet.add(6);
        refSet.add(8);
        // refSet.add("String"); // this will give Exception because <Object>
        System.out.println(refSet);

        // String
        Set<String> refSet2 = new TreeSet<>();
        refSet2.add("D");
        refSet2.add("A");
        refSet2.add("Z");
        refSet2.add("J");
        refSet2.add("Y");
        refSet2.add("T");
        refSet2.add("B");
        refSet2.add("C");
        // refSet2.add(123); // this will give you compilation error because <String>
        System.out.println(refSet2);

        /* this is not allowed as object is non-comparable
        Set<Person> refSet3 = new TreeSet<>();
        refSet3.add(new Person(1, "Han")); */
    }
}
