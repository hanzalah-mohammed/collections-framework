package Collection.Queue.PriorityQueue;

// PriorityQueue
// PriorityQueue can access the first element only

import java.util.PriorityQueue;

public class Example1 {
    public static void main(String[] args) {
        PriorityQueue<Integer> refQueue = new PriorityQueue<>();

        refQueue.add(4);
        refQueue.add(3);
        refQueue.add(6);
        refQueue.add(1);
        refQueue.add(7);
        refQueue.add(9);
        refQueue.add(8);
        refQueue.add(4);

        System.out.println(refQueue);
        System.out.println("Head: " + refQueue.peek()); // can check the first element only

        refQueue.poll(); // can remove the first element only
        System.out.println(refQueue);
    }
}
