## Java Deque Interface
_Java Deque Interface is a linear collection that supports element insertion and removal at both ends. Deque is an acronym for "double ended queue"._

### ArrayDeque class
The important points about ArrayDeque class are:
- Unlike Queue, we can add or remove elements from both sides.
- Null elements are not allowed in the ArrayDeque.
- ArrayDeque is not thread safe, in the absence of external synchronization.
- ArrayDeque has no capacity restrictions.
- ArrayDeque is faster than LinkedList and Stack.