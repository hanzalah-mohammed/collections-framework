package Collection.Queue.ArrayDeque;

// ArrayDeque
// Deque can access the first and last element

import java.util.ArrayDeque;

public class Example1 {
    public static void main(String[] args) {

        ArrayDeque<Integer> refQueue = new ArrayDeque<>();
        refQueue.add(6);
        refQueue.add(1);
        refQueue.add(7);
        refQueue.add(9);
        refQueue.add(8);
        refQueue.add(4);
        refQueue.addFirst(4); // deque can add on the first element,
        refQueue.addLast(3); // as well as on the last element

        System.out.println(refQueue);
        System.out.println("First: " + refQueue.peekFirst()); // can check the first element
        System.out.println("Last: " + refQueue.peekLast()); // as well as last

        refQueue.removeFirst(); // can remove the first element only
        refQueue.removeLast(); // as well as last
        System.out.println(refQueue);
    }
}

