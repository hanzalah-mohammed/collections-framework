package Data;

import java.util.Objects;

public class Person{
    public int ID;
    public String name;

    public Person(int ID, String name) {
        this.ID = ID;
        this.name = name;
    }

    @Override
    public String toString() {
        return ID + " " + name;
    }

    // new created objects will have their own memory address.
    // to check objects in list with same values, we must generate hashCode() and equals() in Object class.
    // This is to remove duplicates when using Set Collections also.

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Person person = (Person) o;
        return ID == person.ID && Objects.equals(name, person.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(ID, name);
    }
}
