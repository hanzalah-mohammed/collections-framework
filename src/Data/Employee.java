package Data;

public class Employee {

    public int ID;
    public String name;
    public double salary;

    public Employee(int ID, String name, double salary) {
        this.ID = ID;
        this.name = name;
        this.salary = salary;
    }

    @Override
    public String toString() {
        return ID + " " + name + " " + salary;
    }
}
